from random import randint

name = input("Hi! What is your name?")


for guess_number in range(1,6):
    guess_month = randint(1,12)
    guess_year = randint(1924,2004)

    print("Guess", guess_number, ": Were you born in",
            guess_month, "/", guess_year, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("This is hopeless!!!! Gotta run!")
        exit()
    else:
        print("Dang it! Let me try again")
